<?php
    session_start();

    //connect to the database
    $pdo = new PDO('mysql:host=localhost;dbname=malife', 'root', '');

        $statement = $pdo->prepare("SELECT * FROM tbl_user");
        $statement->execute(array()); 
         
         //Checks if th requestmethod is right
         if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST))
         {
           $isInputValid = true;  
           $username = $_POST["signUpUsername"];
           $password = $_POST["signUpPassword"];

           //hashes the password
           $hashed_password = password_hash($password, PASSWORD_DEFAULT);

            if ($statement->rowCount() > 0) {    
                //for every row in table
                while($row = $statement->fetch()) {
                        
                    //checks if the username already exists or password is not long enough
                    if ($username == strip_tags($row["username"]) || strlen($password) < 8) {
                        //one thing is false so user input is invalid
                        $isInputValid = false;
                        header("Location: ../../buisness/php/signup.php");
                    }                 
                }
            } 
       
            //if everything is valid write it to the database
            if ($isInputValid) {
                $entry = $pdo->prepare("INSERT INTO tbl_user (username, password)
                VALUES (?, ?)");
                $entry->execute(array($username, $hashed_password)); 

                header("Location: ../../buisness/php/signin.php");
            }
           
         }  
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="CSS\style.css" />
    <title>Validating</title>
</head>
<body>
    
</body>
</html>