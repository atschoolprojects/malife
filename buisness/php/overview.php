<?php
  session_start();
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Tabcontent -->
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <title>MaLife - Overview</title>

    <!-- Meta -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- CSS -->
    <link rel="stylesheet" href="../../presentation/css/style.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>

    <!-- Script -->
    <script src="../js/script.js"></script>
    <script src="https://kit.fontawesome.com/025b998bae.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <script>
        //Navbar for mobile divices
        $(document).ready(function(){
        $('.sidenav').sidenav();
        });
    </script>
  </head>
    <div class="navbar-fixed">
        <nav class="nav-wrapper">
        <a href="#" class="sidenav-trigger right" data-target="mobile-links"><i class="material-icons">menu</i></a>
        <a class="brand-logo center">MaLife</a>
            <ul class="right hide-on-med-and-down">
            <li><a href="logout.php">Sign out</a></li>
            </ul>
        </nav>
    </div>
        
    <ul class="sidenav" id="mobile-links">
        <li><a href="logout.php">Sign out</a></li>
    </ul>

  <body>
    <main>
        <?php
            if($_SESSION["UserID"] != null){
                $pdo = new PDO('mysql:host=localhost;dbname=malife', 'root', '');
            
            $statement = $pdo->prepare("SELECT * FROM `tbl_entry` WHERE fk_user = ? ORDER BY date DESC");
            $statement->execute(array($_SESSION["UserID"]));

              //creates the table with all entries
              if ($statement->rowCount() > 0) {
                echo '
                  <div class="row">                
                ';
                while($row = $statement->fetch()) {
                echo '
                <div class="col s12 m3">
                  <a href="entry.php?editID=' .strip_tags($row["entry_ID"]). '" style="color: black;">
                    <div class="card">
                      <div class="card-image">
                        <img src="../../presentation/img/logo.png" height="300" width="100">
                        <span class="card-title black">' .strip_tags($row["date"]). '</span>
                      </div>
                      <div class="card-content">
                        <p>' .substr((strip_tags($row["content"])), 0, 45). '...'. '</p>
                      </div>
                    </div>
                  </a>
                </div>
                ';
                }
                echo '
                  </div>
                ';
              }
            }
            else{
              //The Sesseionvariable is not set
              header("Location: ../../presentation/html/index.html");
            }
        ?>
        <!-- Space after table -->
        <div class="row"></div>

        <div class="row">
          <div id="add" class="col s10 m11"></div>
            <!-- Add button -->
            <a class="btn-floating btn-large waves-effect waves-light red" href="entry.php"><i class="fas fa-plus"></i></a>
        </div>
    </main>
  </body>
</html>
