<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Tabcontent -->
        <link rel="shortcut icon" href="img/favicon.ico"/>
        <title>MaLife - Sign in</title>

        <!-- Meta -->
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- CSS -->
        <link rel="stylesheet" href="../../presentation/css/style.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>

        <!-- Script -->
        <script src="../../buisness/js/script.js"></script>
        <script src="https://kit.fontawesome.com/025b998bae.js" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

        <script>
            //Navbar for mobile divices
            $(document).ready(function(){
            $('.sidenav').sidenav();
            });
        </script>
    </head>
<body>
    <header>
        <div class="navbar-fixed">
        <nav class="nav-wrapper">
          <a href="#" class="sidenav-trigger right" data-target="mobile-links"><i class="material-icons">menu</i></a>
          <a href="../../presentation/html/index.html" class="left hide-on-med-and-down"><i class="fas fa-home"></i></a>
          <a class="brand-logo center">MaLife</a>
            <ul class="right hide-on-med-and-down">
              <li><a href="../../buisness/php/signin.php">Sign in</a></li>
              <li><a href="../../buisness/php/signup.php">Sign up</a></li>
            </ul>
        </nav>
      </div>
      
      <ul class="sidenav" id="mobile-links">
        <li><a href="../../presentation/html/index.html">Home</a></li>
        <li><a href="../../buisness/php/signin.php">Sign in</a></li>
        <li><a href="../../buisness/php/signup.php">Sign up</a></li>
      </ul>
    </header>
    <main>
        <h1>Sign in</h1>
        <form action="../../dataabstraction/commands/signinValidation.php" method="post">

      <!-- Space from top -->
      <div style="margin-top: 15vh"></div>

        <!-- Username -->
        <div class="row">
            <div class="input-field col s8 xl6 offset-s2 offset-xl3">
                <input id="signInUsername" name="signInUsername" type="text" required class="validate" />
                <label for="signInUsername">Username</label>
            </div>
        </div>

        <!-- Password -->
        <div class="row">
            <div class="input-field col s8 xl6 offset-s2 offset-xl3">
                <input id="signInPassword" name="signInPassword" type="password" required class="validate" />
                <label for="signInPassword">Password</label>
            </div>
        </div>

        <!-- Link to signup.php -->
        <div class="row">
            <div class="center">
                <p class="center-align">
                <a href="signup.php" style="text-align: right">Don't have an account? Sign up <i class="fas fa-arrow-right"></i></a>
                </p>
            </div>
        </div>

        <!-- Submit button -->
        <div class="row">
            <div class="col s1 offset-s5 offset-m7"></div>
            <button class="btn waves-effect waves-light red lighten-2" type="submit" name="action">
                Sign in<i class="material-icons right">send</i>
            </button>
        </div>
        </form>
</main>
</body>
</html>