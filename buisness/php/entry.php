<?php
  session_start();
  if($_SESSION["UserID"] != null){
    $pdo = new PDO('mysql:host=localhost;dbname=malife', 'root', '');
    $id = null;
    $date = "";
    $content = "";

    //If the entryID is set the user edits this entry
    if (isset($_GET['editID'])) {
      $id = $_GET['editID'];

      $statement = $pdo->prepare("SELECT * FROM `tbl_entry` WHERE fk_user = ? AND entry_ID = ?");
      $statement->execute(array($_SESSION["UserID"], $id)); 

      while($row = $statement->fetch()) {
        //fill in the values from the database into the variables
        $date = strip_tags($row["date"]);
        $content = strip_tags($row["content"]);                  
      }
    } 
  }
  else{
    //The Sesseionvariable is not set
    header("Location: overview.php");
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
        <!-- Tabcontent -->
        <link rel="shortcut icon" href="img/favicon.ico"/>
        <title>MaLife - Entry</title>

        <!-- Meta -->
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- CSS -->
        <link rel="stylesheet" href="../../presentation/css/style.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>

        <!-- Script -->
        <script src="../../buisness/js/script.js"></script>
        <script src="https://kit.fontawesome.com/025b998bae.js" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

        <script>
            //Navbar for mobile divices
            $(document).ready(function(){
            $('.sidenav').sidenav();
            });

            //Datepicker
            $(document).ready(function(){
              $('.datepicker').datepicker();
            });
        </script>
    </head>
  <body>

  <header>
      <div class="navbar-fixed">
        <nav class="nav-wrapper">
          <a href="#" class="sidenav-trigger right" data-target="mobile-links"><i class="material-icons">menu</i></a>
          <a href="overview.php" class="right hide-on-med-and-down"></i>Overview</a>
          <a class="brand-logo center">MaLife</a>
        </nav>
      </div>
      
      <ul class="sidenav" id="mobile-links">
        <li><a href="overview.php">Overview</a></li>
      </ul>
    </header>
    
  <div style="margin-top: 15vh"></div>

    <form action="addEntry.php" method="post">
      <div class="row">

        <!-- Date -->
        <div class="input-field col s11 m3 offset-m1">
          <input id="date" name="date" type="text" class="datepicker" required value= "<?php echo $date; ?>">
          <label for="date">Date</i></label>
        </div>

      </div>
      <div class="row">
      
        <!-- Content -->
        <div class="input-field col s11 m10 offset-m1">
          <textarea id="content" name="content" required class="materialize-textarea"><?php echo $content; ?></textarea>
          <label for="content">Content</label>
        </div>

      </div>

      <div class="row">

        <!-- Cancel button -->
        <div class="col s3 m1 offset-s6 offset-m9">
            <button class="btn waves-effect waves-light red lighten-2"/>Cancel</button>
        </div>

        <!-- Save button -->
        <div class="col s3 m1">
          <button class="btn waves-effect waves-light red lighten-2" type="submit" name="action">Save</button>
        </div>

        <!-- Hidden entryID -->
        <input type="hidden" name="entryID" value= "<?php echo $id; ?>">

      </div>  
    </form>
  </body>
</html>
